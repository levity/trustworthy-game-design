
# Table of Contents

1.  [Purpose of this document](#org8a407e3)
    1.  [What this document is not](#org5557a39)
2.  [What is a "trustworthy" game?](#orgc30a4c3)
    1.  [What's a game?](#org24e04ba)
        1.  [Game Description Language](#org3672e3c)
    2.  [What is trust in a game context?](#org8f0686a)
        1.  [Trust is a bet](#org9a51177)
    3.  [A novel trust metric?](#orgfae32e8)
    4.  [Other approaches to trust metrics](#org1543cf4)
    5.  [How may we evaluate the "trustworthiness" of a game?](#orgcf4642d)
        1.  [FreeCell proof](#org04c6c30)
        2.  [Klondike proof](#orgd480a6e)
3.  [Why is trust an important property of games?](#org5f194fb)
    1.  [Trust environments](#orgf21a1ca)
    2.  [Games are important](#org4543c22)
    3.  [Trust is important](#org2e91e42)
4.  [How may we model trust as a property of games?](#org24feb13)
    1.  [Trust in the game](#org6ffc9d0)
    2.  [Trust among players](#org82c5800)
5.  [How to construct trustworthy games?](#org0a45c5e)
    1.  [Properties of statistical trust](#org5f65bab)
        1.  [Properties that depend on trust](#org11927ee)
    2.  [Patterns of trust](#orgd07300e)
        1.  [Chain](#org0b27fcf)
        2.  [Tree](#orge1a68d8)
        3.  [Sieve](#orgd1e174e)
    3.  [Patterns against trust](#orgd8ba043)
        1.  [on games](#org032203f)
        2.  [between players](#orgd74ddb7)
        3.  [Obfuscation](#org045d661)
6.  [Application to "unconventional" games](#org2fbf245)
    1.  [Computer programs](#org2a845f6)
7.  [Practical examples](#orgfd82dca)
    1.  ["Assholes and Infiltrators"](#org610e23e)
        1.  [What is "Assholes and Infiltrators"?](#org17d6c48)
        2.  [How can A&I be made trustworthy?](#orgc3f4580)
        3.  [Identifying desirable properties](#org186aef7)
        4.  [Generating useful wagers from properties](#orgd07e518)
        5.  [Measuring wager winrates and confidence intervals](#org6f9e08b)
    2.  [Faircoin](#org9b3c36d)


<a id="org8a407e3"></a>

# Purpose of this document

This document is intended in part as a starting point to learning aspects of game theory, and to that end it contains lots of links. 
The first link is to <https://salsa.debian.org/levity/trustworthy-game-design> - this is the "canonical home" of this document. If you wish to add to, edit or report problems with this document, you can do so there.

The document also explores a probabilistic trust metric. <https://en.wikipedia.org/wiki/Trust_metric> explains what that is. The metric discussed in this document is one intended to generalize to any game agent, not just human ones.

Finally, the document will introduce and document a library of software for working with the introduced trust metric.


<a id="org5557a39"></a>

## What this document is not

This document does not purport relevance to any branch of economics nor to "friendly AI" research.


<a id="orgc30a4c3"></a>

# What is a "trustworthy" game?


<a id="org24e04ba"></a>

## What's a game?

Games can be represented mathematically, using "game theory"

-   <https://en.wikipedia.org/wiki/Game_theory>
-   <https://en.wikipedia.org/wiki/Combinatorial_game_theory>
-   <https://github.com/melisgl/micmac>
-   <https://en.wikipedia.org/wiki/Gaming_mathematics>

Trust can also be represented mathematically. This can be done in many different ways.

In this document a "game" means a list of rules. This is slightly different from the "according to Hoyle" definition from game theory. Probably it will change.
A rule is a statement which defines a game entity or an interaction among entities in the game. A rule is itself an "entity". An entity is any identifier to which a rule refers.


<a id="org3672e3c"></a>

### Game Description Language

<http://games.stanford.edu/games/gdl.html>
Game Description Language is a logic language designed at Stanford University's General Game Player project. It's a mathematical formal language for describing games.


<a id="org8f0686a"></a>

## What is trust in a game context?

There are at least two distinct contexts within games where trust comes into play.

A player may trust, or not trust, the game itself; or a player may trust, or not trust, another player.
This inter-player trust may generalize to non-player agents. Let's find out!


<a id="org9a51177"></a>

### Trust is a bet


<a id="orgfae32e8"></a>

## A novel trust metric?

This paper is about the idea that trust may be represented as a wager.

When an agent Bob to trust another agent Carol, Bob may bet on an assertion made by Carol. An assertion is information deliberately conveyed by Carol. 

An "assertion" may be modeled as an argument to a function that returns true or false. The assertion and its evaluating function work within the context of the game.

Bob might consider Carol trustworthy to the extent that prior assertions made by Carol have returned "true" from the evaluating function, if Bob is an unsophisticated robot (like in the movie The Black Hole).

Similarly, we can call a game "trustworthy" to the extent of our confidence in  assertions about properties of the game. For example, Klondike is not a trustworthy game in terms of a given game being winnable under optimum play; FreeCell is a trustworthy game in this sense.
Klondike has >100% winnability, FreeCell has 100%.

Subjectively, a game is "trustworthy" if participants in the game can trust that play according to the rules they know will give results consonant with their predictions. In a deterministically-trustworthy game, an agent that plays perfectly can expect to win with probability 1.


<a id="org1543cf4"></a>

## Other approaches to trust metrics

put links
<https://en.wikipedia.org/wiki/Advogato>
<https://github.com/steevithak/mod_virgule>
<http://virgule.sourceforge.net/>
<https://en.wikipedia.org/wiki/Reputation_system>
<https://en.wikipedia.org/wiki/PageRank>
<https://en.wikipedia.org/wiki/TrustRank>
<https://en.wikipedia.org/wiki/EigenTrust>


<a id="orgcf4642d"></a>

## How may we evaluate the "trustworthiness" of a game?

It's pretty straightforward to evaluate Klondike vs. FreeCell in terms of solved-ness[wikipedia, 'solved game'].
 We can show that at least one sequence of valid moves exists in FreeCell from any valid initial state to its win state.

On the other hand, we can show even more easily that at least one valid initial Klondike state exists from whence no sequence of valid moves will reach the win state.


<a id="org04c6c30"></a>

### FreeCell proof

Similarly, we can show that Klondike's win state is not reachable from some initial states.


<a id="orgd480a6e"></a>

### Klondike proof

Since trust is a bet, we can say that a game is trustworthy to the extent that outcomes of bets on game outcomes are statistically predictable based on the rules of the game.


<a id="org5f194fb"></a>

# Why is trust an important property of games?

Trust as a property of games is analogous to trust among players in that each takes the form of a wager on an assertion. In the latter case players bet that an assertion about another player is true; in the former case players bet that an assertion about the game is true.

Without trust in the game, trust **within the context of the game** cannot be established.


<a id="orgf21a1ca"></a>

## Trust environments

A "trust environment" is a trusted context (for example, a trusted game in which a player wishes to establish interplayer trust).


<a id="org4543c22"></a>

## Games are important

Game theory generalizes to any model which consists of a set of rules. This means things like laws and computer software are "games". To the extent that we can make these things more trustworthy, we improve outcomes for their users. 

A foundational work in the field of game theory is Von Neumann and Morgenstern's **Theory of Games and Economic Behavior** which, as the title suggests, is a game-theoretic examination of economics. A trustworthy economy might be a good thing.


<a id="org2e91e42"></a>

## Trust is important

If we can't know what the results of our interactions with law will be, how can we choose to act legally? If we can't so choose, how is justice possible?
If we can't predict the output of a machine, why start it?


<a id="org24feb13"></a>

# How may we model trust as a property of games?

If we treat trust as a type of wager then we can take advantage of existing maths about bets.

Verification is a process by which a wager is evaluated by comparing its assertion to a measurement. 
An "assertion" is a string in a symbolic language that can be evaluated by some machine, producing a value comparable against a corresponding part of the game's environment. A useful assertion is one that can be understood by some agent as an operation on its model of the game.


<a id="org6ffc9d0"></a>

## Trust in the game


<a id="org82c5800"></a>

## Trust among players


<a id="org0a45c5e"></a>

# How to construct trustworthy games?

In order to reason about trust in games, we can construct proofs around the games based on their properties. 


<a id="org5f65bab"></a>

## Properties of statistical trust

If we treat "trust" as an operation using "<3" as its token, such that A<3B means "agent A makes wager B" 

-   Distribution
-   Association
-   Commutation: A<3B != B<3A


<a id="org11927ee"></a>

### Properties that depend on trust

1.  "Fairness" and structural advantage

    -   **Fair game:** game in which every player can trust the rules. That is, if a player bets that its utation will increase as a result of a set of moves, and the rules state that such a set of moves will in fact increase the imputation of the player that makes it, then the bet should always win.
    
    -   **A Foul game:** game which is not fair. Foul games are not winnable except by accident, because players do not have enough information to control their imputations. Therefore bets on the results of moves based on the rules will not reliably win.
    
    -   **Fair play:** play that co-operates with the rules. To compromise the rules is usually called "cheating", but there are other, non-cheating ways to compromise the rules such as some forms of meta-gaming and semantic exploits. Accidental compromise of the rules is not cheating, but also not fair play.
    
    -   **Foul play:** is play which is not fair.
    
    When a rule set allows or forces foul play, we can say that it is <dfn>compromised</dfn>. Some rule sets start out compromised by design or by accident. These rule sets cannot define fair games, unless the nature of play allows modification of the initial rule set.  By these definitions, luck-based games like blackjack or craps are not "fair" games except in a statistical sense: a predictable payoff matrix can be constructed, but bets on individual moves or even entire games are not predictable.
    
    Sometimes it's tempting for a player to compromise the rules in such a way as to allow that player to gain imputational advantage (for example, scoring foul points with a "cheap shot".)
     When this happens, it's in the interest of the other players to know that it has happened. If players cannot detect compromise of the rules, they cannot verify the fairness of the game and must assume unfairness. The ability to detect such compromises is necessary to security.  


<a id="orgd07300e"></a>

## Patterns of trust


<a id="org0b27fcf"></a>

### Chain

If an agent1 trusts that agent2 trusts that agent3 trusts $assertion, then you have a "trust chain" of length 3.


<a id="orge1a68d8"></a>

### Tree

If a trust chain contains a link with a compound assertion (e.g., "agent(n-1) trusts ((agent(n-2) trusts foo) AND (bar))" ) then we call it a "trust tree"- a collection of trust chains with a common "root" element.


<a id="orgd1e174e"></a>

### Sieve

A function that takes a "trust tree" (a list of trust chains with a common root) and returns another which contains no chain which has a link which scores above that metric is a "trust sieve".


<a id="orgd8ba043"></a>

## Patterns against trust


<a id="org032203f"></a>

### on games

-   **Exploit:** a situation where the rule-compliant actions break rules
-   **Privilege:** rules apply differently to different players


<a id="orgd74ddb7"></a>

### between players

-   **Forced Trust:** a situation in which an actor demands trust as a condition of continued cooperation. Sometimes it is coupled with trust-faking.
-   **Trust-faking:** is a feigned trust in which the faking actor does not intend to cooperate in the trusting party's payoff.


<a id="org045d661"></a>

### Obfuscation


<a id="org2fbf245"></a>

# Application to "unconventional" games


<a id="org2a845f6"></a>

## Computer programs


<a id="orgfd82dca"></a>

# Practical examples


<a id="org610e23e"></a>

## "Assholes and Infiltrators"


<a id="org17d6c48"></a>

### What is "Assholes and Infiltrators"?

Assholes and Infiltrators is a social game invented by Lisha Sterling. 
It's an informal conversation game played by people in high-stress, high-stake social environments which are subject to attack from outside. It works similarly to Mafia games, but is relatively unstructured.
The basic procedure is: Alice says of Bob, in a social context to which Bob is not privy, that Bob is an infiltrator because of event Foo. Carol replies with corroborating statements or statements illustrating why Foo means that Bob is an asshole but not necessarily an infiltrator.


<a id="orgc3f4580"></a>

### How can A&I be made trustworthy?

In order to make any game trustworthy, it is necessary to identify what aspects of the game are desired wager objects.


<a id="org186aef7"></a>

### Identifying desirable properties

In the case of A&I it might be desirable to ensure the following properties:

1.  All valid games of A&I result in an unambiguous indication of "infiltrator" or "not infiltrator".
2.  Any game which does not result in such an indication is unambiguously marked as invalid (or untrustworthy).

These are unlikely to be useful properties to aim for in real-world social games, and are unrealistic in the particular case of A&I. They are used here for illustrative purposes only, to show what can be done.


<a id="orgd07e518"></a>

### Generating useful wagers from properties


<a id="org6f9e08b"></a>

### Measuring wager winrates and confidence intervals


<a id="org9b3c36d"></a>

## Faircoin

